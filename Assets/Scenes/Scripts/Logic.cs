using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Resight;

public class Logic : MonoBehaviour
{
    [SerializeField] Text statusLabel;

    public GameObject _spherePrefab;
    private GameObject _mySphere;

    void Awake()
    {
        LibResight.Instance.OnStatus += (EngineState status, System.IntPtr ctx) =>
        {
            if (status == EngineState.Init)
            {
                statusLabel.text = "Mapping...";
            }

            if (status == EngineState.Mapping)
            {
                statusLabel.text = "Mapped";
            }
        };

    }
    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine(SpawnSphere());
    }

    IEnumerator SpawnSphere()
    {
        yield return new WaitForSeconds(1);
        _mySphere = Instantiate(_spherePrefab, new Vector3(0, 0, 1), Quaternion.identity);
    }
    // Update is called once per frame
    void Update()
    {
        if (_mySphere == null) return;
        
        float movementSpeed = 0.01f;

        if (Input.touchCount == 1) {
            _mySphere.transform.Translate(
                Input.GetTouch(0).deltaPosition.x * movementSpeed * Time.deltaTime,
                0,
                Input.GetTouch(0).deltaPosition.y * movementSpeed * Time.deltaTime
            );
        }
    }
}
