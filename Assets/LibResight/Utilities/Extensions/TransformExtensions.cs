﻿using UnityEngine;
using System.Collections;
using Resight;

namespace Resight.Utilities.Extensions
{
    internal static class TransformExtensions
    {
        private static Quaternion ToQuaternion(this RSRotation rot)
        {
            Quaternion rotationQuat = Quaternion.identity;

            switch (Screen.orientation)
            {
                case ScreenOrientation.Portrait:
                    rotationQuat = Quaternion.AngleAxis(90.0f, Vector3.forward);
                    break;
                case ScreenOrientation.LandscapeLeft:
                    break;
                default:
                    break;
            }

            Quaternion res = new Quaternion(rot.x, rot.y, -rot.z, -rot.w);

            return res * rotationQuat;
        }

        private static RSRotation ToRSRotation(this Quaternion quat)
        {
            Quaternion rotationQuat = Quaternion.identity;

            switch (Screen.orientation)
            {
                case ScreenOrientation.Portrait:
                    rotationQuat = Quaternion.AngleAxis(-90.0f, Vector3.forward);
                    break;
                case ScreenOrientation.LandscapeLeft:
                    break;
                default:
                    break;
            }

            var resightQuat = quat * rotationQuat;

            return new RSRotation
            {
                x = resightQuat.x,
                y = resightQuat.y,
                z = -resightQuat.z,
                w = -resightQuat.w
            };
        }

        public static RSPose ToRSPose(this Matrix4x4 matrix)
        {
            var position = matrix.GetColumn(3);
            var rotation = Quaternion.LookRotation(matrix.GetColumn(2), matrix.GetColumn(1));
            return new RSPose
            {
                pos = new RSPosition
                {
                    x = position.x,
                    y = position.y,
                    z = -position.z
                },
                rot = rotation.ToRSRotation()
            };
        }

        public static RSPose ToRSPose(this Transform transform)
        {
            return new RSPose
            {
                pos = new RSPosition
                {
                    x = transform.localPosition.x,
                    y = transform.localPosition.y,
                    z = -transform.localPosition.z
                },
                rot = transform.localRotation.ToRSRotation()
            };
        }

        public static void ToTransform(this RSPose pose, Transform transform)
        {
            transform.localPosition = new Vector3
            {
                x = pose.pos.x,
                y = pose.pos.y,
                z = -pose.pos.z
            };

            transform.localRotation = pose.rot.ToQuaternion();
        }

        public static Matrix4x4 ToMatrix4x4(this RSPose pose)
        {
            var position = new Vector3(pose.pos.x, pose.pos.y, -pose.pos.z);
            var rotation = pose.rot.ToQuaternion();
            var mat = Matrix4x4.identity;
            mat.SetTRS(position,
                       rotation,
                       Vector3.one);
            return mat;
        }

        public static Matrix4x4 ToMatrix4x4(this Transform transform)
        {
            return Matrix4x4.TRS(transform.position, transform.rotation, Vector3.one);
        }

    } // ResightExtensions
}
