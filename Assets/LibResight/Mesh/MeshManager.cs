﻿using UnityEngine;
using System;
using System.Collections.Generic;

namespace Resight.Mesh
{
    public class MeshManager : MonoBehaviour
    {
        [Tooltip("Prefab should include a MeshBlock component.")]
        [SerializeField] private GameObject meshBlockPrefab;

        [Space(10)]

        [Header("Physics")]
        [Tooltip("Disable the MeshBlock's collider, if exist.")]
        [SerializeField] private bool disableCollider = false;

        [Tooltip("Seconds to wait before updating the MeshCollider of an updated MeshBlock.")]
        [SerializeField] private float recalculateColliderInterval = 0.1f;


        //[Tooltip("Disable the MeshBlock's renderer, if exist.")]
        //[SerializeField] private bool disableRenderer = false;

        public enum RENDERING_OPTIONS
        {
            None = 0,
            Normals = 1,
            Colors = 2,
            Shadows = 3,
            ColorsAndShadows = 4,
            Prefab = 5
        }

        [Header("Rendering")]
        public RENDERING_OPTIONS renderingOptions = RENDERING_OPTIONS.ColorsAndShadows;

        [HideInInspector] public Material materialNormals;
        [HideInInspector] public Material materialColors;
        [HideInInspector] public Material materialShadows;
        [HideInInspector] public Material materialColorsAndShadows;

        private readonly Dictionary<ulong, MeshBlock> meshBlocks_ = new();

        private void Awake()
        {
            LibResight.Instance.OnStatus += OnStatus;
            LibResight.Instance.OnMeshBlockAdded += OnMeshBlockAdded;
            LibResight.Instance.OnMeshBlockRemoved += OnMeshBlockRemoved;
            LibResight.Instance.OnMeshBlockUpdated += OnMeshBlockUpdated;
        }

        private void Update()
        {
            if (Input.GetMouseButtonDown(1)) {
                if (transform.localScale.x == 1) {
                    float a = 0.15f;
                    transform.localScale = new Vector3(a, a, a);
                } else {
                    transform.localScale = new Vector3(1, 1, 1);
                }
            }
        }

        private void OnStatus(EngineState status, IntPtr ctx)
        {
            if (status == EngineState.Init) {
                foreach (var mesh in meshBlocks_.Values) {
                    Destroy(mesh.gameObject);
                }
                meshBlocks_.Clear();
            }
        }

        private void OnMeshBlockAdded(RSMeshBlockAddedEvent blockAddedEvent, IntPtr ctx)
        {
            var go = Instantiate(meshBlockPrefab, transform);
            go.name = $"MeshBlock {blockAddedEvent.block_id}";

            var pos = blockAddedEvent.block_position;
            go.transform.localPosition = new Vector3(pos.x, pos.y, pos.z);

            if (!go.TryGetComponent(out MeshBlock meshBlock)) {
                meshBlock = go.AddComponent<MeshBlock>();
            }

            meshBlock.Bounds = new Bounds(
                new Vector3(
                    0.5f * blockAddedEvent.block_size.x,
                    0.5f * blockAddedEvent.block_size.y,
                    0.5f * blockAddedEvent.block_size.z),
                new Vector3(
                    blockAddedEvent.block_size.x,
                    blockAddedEvent.block_size.y,
                    blockAddedEvent.block_size.z)
                );

            meshBlock.recalculateColliderInterval = recalculateColliderInterval;

            if (go.TryGetComponent(out MeshRenderer meshRenderer)) {
                switch (renderingOptions) {
                    case RENDERING_OPTIONS.None:
                        meshRenderer.enabled = false;
                        break;
                    case RENDERING_OPTIONS.Normals:
                        meshRenderer.sharedMaterial = materialNormals;
                        break;
                    case RENDERING_OPTIONS.Colors:
                        meshRenderer.sharedMaterial = materialColors;
                        break;
                    case RENDERING_OPTIONS.Shadows:
                        meshRenderer.sharedMaterial = materialShadows;
                        break;
                    case RENDERING_OPTIONS.ColorsAndShadows:
                        meshRenderer.sharedMaterial = materialColorsAndShadows;
                        break;
                    case RENDERING_OPTIONS.Prefab:
                        // nothing to do, we will use the material set in MeshBlockPrefab
                        break;
                }
            }

            if (disableCollider && go.TryGetComponent(out MeshCollider meshCollider)) {
                meshCollider.enabled = false;
            }

            meshBlocks_[blockAddedEvent.block_id] = meshBlock;
        }

        private void OnMeshBlockRemoved(ulong block_id, IntPtr ctx)
        {
            meshBlocks_.Remove(block_id);
        }

        private void OnMeshBlockUpdated(ulong block_id, IntPtr ctx)
        {
            if (meshBlocks_.TryGetValue(block_id, out MeshBlock meshBlock)) {
                meshBlock.UpdateData(block_id);
            }
        }
    }
}
