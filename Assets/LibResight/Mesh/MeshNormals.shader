﻿Shader "Resight/MeshNormals"
{
    SubShader
    {
        Pass
        {            
            LOD 150
            ZTest LEqual        
            ZWrite On

            HLSLPROGRAM

            #pragma vertex vert
            #pragma fragment frag
            #include "UnityCG.cginc"


            struct appdata
            {
                float4 vertex : POSITION;
                float3 normal : NORMAL;
            };

            struct v2f
            {
                float4 pos : SV_POSITION;
                half3 normal : TEXCOORD0;
            };

            struct fragment_output
            {
                half4 color : SV_Target;
            };

            v2f vert(appdata v)
            {
                v2f o;
                o.pos = UnityObjectToClipPos(v.vertex);
                o.normal = UnityObjectToWorldNormal(v.normal);
                return o;
            }

            fragment_output frag(v2f i)
            {
                fragment_output o;
                o.color.rgb = i.normal * 0.5 + 0.5;
                o.color.a = 1;
                return o;
            }

            ENDHLSL
        }
    }

    FallBack "VertexLit"
}