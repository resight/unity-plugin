﻿using UnityEngine;
//using System.Collections;
using System;
using System.Collections.Generic;
using UnityEngine.XR.ARFoundation;
using UnityEngine.XR.ARSubsystems;
using System.Collections;
using Resight.Utilities.Extensions;
using System.Runtime.InteropServices;

namespace Resight.Persistence
{
    public class EntitiesManager : MonoBehaviour
    {
        public ARSessionOrigin aRSessionOrigin;

        private readonly Dictionary<ulong, RSAnchor> remoteAnchors_ = new();
        private readonly Dictionary<ulong, RSEntity> orphanEntities_ = new();
        private readonly Dictionary<ulong, GameObject> entities_ = new();

        public static EntitiesManager Instance { get; private set; }
        private Transform origin_;
        
        private void Awake()
        {
            if (Instance != null && Instance != this) {
                Debug.LogError("[LibResight] Only one instance of EntitiesManager is allowed");
                return;
            }

            Instance = this;

            LibResight.Instance.OnAnchor += OnAnchor;
            LibResight.Instance.OnEntityAdded += OnEntityAdded;
            LibResight.Instance.OnEntityRemoved += OnEntityRemoved;
            LibResight.Instance.OnEntityPoseUpdated += OnEntityPoseUpdated;
            LibResight.Instance.OnEntityDataUpdated += OnEntityDataUpdated;

            if (aRSessionOrigin == null) {
                aRSessionOrigin = FindObjectOfType<ARSessionOrigin>();
                origin_ = (aRSessionOrigin != null) ? aRSessionOrigin.trackablesParent : transform;
            }            
        }

        // Public API
        public RSAnchor AddAnchor(ulong id, Matrix4x4 transform)
        {
            var anchor = RSCreateAnchor(id, transform.ToRSPose());

            //remoteAnchors[anchor.id] = transform.gameObject;
            remoteAnchors_[id] = anchor;
            RSAddAnchor(anchor);

            return anchor;
        }

        public void AddEntity(GameObject go, RSAnchor anchor)
        {
            var entity = go.GetComponent<SnappedObject>();
            if (!entity) {
                Debug.LogError("[LibResight] AddEntity(): entity does not have a SnappedObject component");
                return;
            }

            if (entities_.ContainsKey(entity.Id)) {
                Debug.LogError("[LibResight] AddEntity(): could not add entity " + entity.Id + " - id already exist");
                return;
            }

            // attach to origin and add to managed entities
            go.transform.SetParent(origin_, true);
            go.transform.hasChanged = false;
            entities_[entity.Id] = go;
            Debug.Log("[LibResight] AddEntity(): id=" + entity.Id + " prefabId=" + entity.PrefabId + ", anchor=" + anchor.id + ", GetInstanceID=" + go.GetInstanceID());
            
            // broadcast
            var data_len = entity.AuxData != null ? entity.AuxData.Length : 0;
            var pose = go.transform.ToRSPose();
            GCHandle gch = GCHandle.Alloc(entity.AuxData, GCHandleType.Pinned);
            RSAddEntity(anchor, entity.PrefabId, pose, gch.AddrOfPinnedObject(), data_len);
            gch.Free();
        }

        public void RemoveEntity(GameObject go, RSAnchor anchor)
        {
            var entity = go.GetComponent<SnappedObject>();
            if (!entity) {
                Debug.LogError("[LibResight] RemoveEntity(): entity does not have a SnappedObject component");
                return;
            }

            if (anchor.id == 0UL)
            {
                anchor = remoteAnchors_[entity.Id];
            }

            Debug.Log("[LibResight] RemoveEntity(): id=" + anchor.id + " prefabId=" + entity.PrefabId);

            RSRemoveEntity(anchor);
        }

        public void UpdateEntityPose(GameObject go, RSAnchor anchor)
        {
            var entity = go.GetComponent<SnappedObject>();
            if (!entity) {
                Debug.LogError("[LibResight] UpdateEntityPose(): entity does not have a SnappedObject component");
                return;
            }

            Debug.Log("[LibResight] UpdateEntityPose() (local): Id=" + entity.Id + ", anchor=" + anchor.id + ", GetInstanceID=" + go.GetInstanceID() + ", pos=" + go.transform.position + ", instanceId=" + entity.Id);

            //TODO: eliminate anchor's pose from local pose and send the 
            if (anchor.id == 0UL)
            {
                anchor = remoteAnchors_[entity.Id];
            }
            var entityLocalPoseMat = anchor.pose.ToMatrix4x4().inverse * go.transform.ToMatrix4x4();

            RSUpdateEntityPose(anchor, entityLocalPoseMat.ToRSPose());
        }

        public void UpdateEntityData(GameObject go, RSAnchor anchor)
        {
            var entity = go.GetComponent<SnappedObject>();
            if (!entity) {
                Debug.LogError("[LibResight] UpdateEntityData(): entity does not have a SnappedObject component");
                return;
            }

            var data_len = entity.AuxData != null ? entity.AuxData.Length : 0;
            Debug.Log("[LibResight] UpdateEntityData() (local): Id=" + entity.Id + ", data_len=" + data_len);

            var id = entity.PrefabId;

            if (anchor.id == 0UL)
            {
                anchor = remoteAnchors_[entity.Id];
            }

            GCHandle gch = GCHandle.Alloc(entity.AuxData, GCHandleType.Pinned);
            RSUpdateEntityData(anchor, gch.AddrOfPinnedObject(), data_len);
            gch.Free();
        }

        private void OnAnchor(RSAnchor anchor, int anchorType, byte weight, IntPtr userdata, IntPtr ctx)
        {
            if (!remoteAnchors_.TryGetValue(anchor.id, out RSAnchor lastUpdate)) {
                // new anchor
                //Debug.Log("[LibResight] HandleOnAnchor(): New remote anchor: anchor.id=" + anchor.id);
                remoteAnchors_.Add(anchor.id, anchor);

                if (orphanEntities_.TryGetValue(anchor.id, out RSEntity entity)) {
                    Debug.Log("[LibResight] HandleOnAnchor(): Reattaching entity " + entity.parentId + " to anchor " + anchor.id);
                    OnEntityAdded(entity, ctx);
                    orphanEntities_.Remove(anchor.id);
                }

                return;
            }

            // Update entity's pose based on anchor's pose
            GameObject entityObj;
            if (entities_.TryGetValue(anchor.id, out entityObj)) {
                var lastAnchorPose = lastUpdate.pose.ToMatrix4x4();
                var newAnchorPose = anchor.pose.ToMatrix4x4();
                var deltaAnchorChange = newAnchorPose * lastAnchorPose.inverse;

                UpdateEntityPose(deltaAnchorChange, entityObj);
                //anchor.pose.ToTransform(entityObj.transform);

                Debug.Log("[LibResight] HandleOnAnchor(): anchor.id=" + anchor.id
                        + ", anchor.pos=" + entityObj.transform.position + ", anchor.rot=" + entityObj.transform.rotation
                        + ", anchor.localPos=" + entityObj.transform.localPosition + ", anchor.localRot=" + entityObj.transform.localRotation);
            }

            // update anchor in remoteAnchors (as it's a struct, we need to readd it to the list)
            remoteAnchors_.Remove(anchor.id);
            remoteAnchors_.Add(anchor.id, anchor);

            //Debug.Log("[LibResight] HandleOnAnchor(): Anchor_id: " + anchor.id + " pose: " + anchor.pose.pos.x + "," + anchor.pose.pos.y + "," + anchor.pose.pos.z + "\n");
        }

        private void OnEntityAdded(RSEntity entity, IntPtr ctx)
        {
            Debug.Log("[LibResight] OnEntityAdded(): entity.id=" + entity.id + ", entity.parentId=" + entity.parentId + ", time=" + Time.time);

            if (entities_.ContainsKey(entity.parentId)) {
                Debug.Log("[LibResight] OnEntityAdded(): ignored, entity already exist");
                return;
            }

            if (HandleOrphanEntity(entity)) {
                return;
            }

            if (entity.id == "") {
                Debug.LogError("[LibResight] OnEntityAdded(): ignored, entity has an empty key");
                return;
            }

            var entityObj = Instantiate(Resources.Load(entity.id, typeof(GameObject))) as GameObject;
            entityObj.transform.SetParent(origin_);
            if (!entityObj.TryGetComponent(out SnappedObject snappedObject)) {
                snappedObject = entityObj.AddComponent<SnappedObject>();
            }
            snappedObject.Id = entity.parentId;
            snappedObject.AuxData = entity.data;

            entities_[entity.parentId] = entityObj;

            UpdateEntityPose(entity);

            //Debug.Log("[LibResight] HandleOnEntityAdded(): entity.id=" + entity.id
                //+ ", entityObj.pos=" + entityObj.transform.position + ", entityObj.rot=" + entityObj.transform.rotation
                //+ ", entityObj.localPos=" + entityObj.transform.localPosition + ", entityObj.localRot=" + entityObj.transform.localRotation
                //+ ", parent.pos=" + entityObj.transform.position + ", parent.rot=" + entityObj.transform.rotation);
        }

        private void OnEntityRemoved(RSEntity entity, IntPtr ctx)
        {
            Debug.Log("[LibResight] OnEntityRemoved(): entity.parentId=" + entity.parentId + ", time=" + Time.time);

            if (entities_.ContainsKey(entity.parentId)) {
                var entityObj = entities_[entity.parentId];
                Destroy(entityObj);
                entities_.Remove(entity.parentId);
            } 
            else if (orphanEntities_.ContainsKey(entity.parentId)) {
                // remove orphan
                orphanEntities_.Remove(entity.parentId);
            }
        }

        private void OnEntityPoseUpdated(RSEntity entity, IntPtr ctx)
        {
            Debug.Log("[LibResight] OnEntityPoseUpdated(): entity.parentId=" + entity.parentId + ", time=" + Time.time);
            if (entities_.ContainsKey(entity.parentId)) {
                UpdateEntityPose(entity);
            }
            else if (orphanEntities_.ContainsKey(entity.parentId)) {
                // update orhpan's pose
                var updatedEntity = orphanEntities_[entity.parentId];
                updatedEntity.pose = entity.pose;
                orphanEntities_[entity.parentId] = updatedEntity;
            }
            else {
                Debug.Log("[LibResight] OnEntityPoseUpdated(): Entity was not found. Adding it.");
                OnEntityAdded(entity, ctx);
            }
        }

        private void OnEntityDataUpdated(RSEntity entity, IntPtr ctx)
        {
            Debug.Log("[LibResight] OnEntityDataUpdated(): entity.parentId=" + entity.parentId + ", time=" + Time.time);
            if (entities_.ContainsKey(entity.parentId)) {
                var entityObj = entities_[entity.parentId];
                //entityObj.GetComponent<IRSEntity>().Decode(entity.data);
            }
            else if (orphanEntities_.ContainsKey(entity.parentId)) {
                var updatedEntity = orphanEntities_[entity.parentId];
                updatedEntity.data = entity.data;
                updatedEntity.dtSize = entity.dtSize;
                orphanEntities_[entity.parentId] = updatedEntity;
            }
            else {
                Debug.Log("[LibResight] OnEntityDataUpdated(): Entity was not found. Adding it.");
                OnEntityAdded(entity, ctx);
            }
        }

        private void UpdateEntityPose(Matrix4x4 anchorMat, GameObject entity)
        {
            var entityMat = entity.transform.ToMatrix4x4();
            //anchor.pose.ToMatrix4x4(out Matrix4x4 anchorMat);
            var entityPoseMat = anchorMat * entityMat;

            var position = entityPoseMat.GetColumn(3);
            entity.transform.SetPositionAndRotation(new Vector3(position.x, position.y, position.z),
                                                    Quaternion.LookRotation(entityPoseMat.GetColumn(2), entityPoseMat.GetColumn(1)));
            entity.transform.hasChanged = false; // so SnappedObject won't send this update back to server
        }

        private bool HandleOrphanEntity(RSEntity entity)
        {
            bool isOrphan = !remoteAnchors_.ContainsKey(entity.parentId);
            if (isOrphan) {
                Debug.Log("[LibResight] HandleOrphanEntity(): saving orphan entity: entity.parentId=" + entity.parentId + ", entity.id=" + entity.id);
                orphanEntities_[entity.parentId] = entity;
            }

            return isOrphan;
        }

        private void UpdateEntityPose(RSEntity entity)
        {
            var anchor = remoteAnchors_[entity.parentId];
            var entityObj = entities_[entity.parentId];
            //var entityMat = Matrix4x4.TRS(entityObj.transform.position, entityObj.transform.rotation, Vector3.one);
            Matrix4x4 anchorMat = anchor.pose.ToMatrix4x4();
            Matrix4x4 entityMat = entity.pose.ToMatrix4x4();
            var entityPoseMat = anchorMat * entityMat;

            var position = entityPoseMat.GetColumn(3);
            Debug.Log("[LibResight] target position: pos=" + position);
            entityObj.transform.SetPositionAndRotation(new Vector3(position.x, position.y, position.z),
                                                    Quaternion.LookRotation(entityPoseMat.GetColumn(2), entityPoseMat.GetColumn(1)));
            entityObj.transform.hasChanged = false; // so SnappedObject won't send this update back to server
        }


#if UNITY_IOS && !UNITY_EDITOR
        [DllImport("__Internal")]
        private static extern void RSAddAnchor(RSAnchor anchor);

        [DllImport("__Internal")]
        private static extern RSAnchor RSCreateAnchor(ulong id, RSPose pose);

        [DllImport("__Internal")]
        private static extern int RSAddEntity(RSAnchor anchor, [MarshalAs(UnmanagedType.LPStr)] string key, RSPose pose, IntPtr data, int size);

        [DllImport("__Internal")]
        private static extern void RSRemoveEntity(RSAnchor anchor);

        [DllImport("__Internal")]
        private static extern void RSUpdateEntityPose(RSAnchor anchor, RSPose pose);

        [DllImport("__Internal")]
        private static extern void RSUpdateEntityData(RSAnchor parent, IntPtr data, int size);
#else
        private static void RSAddAnchor(RSAnchor anchor) {}

        private static RSAnchor RSCreateAnchor(ulong id, RSPose pose) {return new RSAnchor();}

        private static int RSAddEntity(RSAnchor anchor, [MarshalAs(UnmanagedType.LPStr)] string key, RSPose pose, IntPtr data, int size) {return 0;}

        private static void RSRemoveEntity(RSAnchor anchor) { }

        private static void RSUpdateEntityPose(RSAnchor anchor, RSPose pose) {}

        private static void RSUpdateEntityData(RSAnchor parent, IntPtr data, int size) {}
#endif
    }
}
