﻿using UnityEngine;
using System.Collections;
using System;

namespace Resight.Persistence
{

    public class SnappedObject : MonoBehaviour
    {
        static bool isQuitting = false;

        static bool WantsToQuit()
        {
            Debug.Log("Player is quitting");
            isQuitting = true;
            return true;
        }

        [RuntimeInitializeOnLoadMethod]
        static void RunOnStart()
        {
            Application.wantsToQuit += WantsToQuit;
        }
        /// <summary>
        /// An Id that represents the instance of the object, shared between all peers in the session
        /// </summary>
#if UNITY_EDITOR
        [ReadOnly]
#endif
        [SerializeField]
        private long _instanceId = 0L;

        private bool _isRegistered = false;

        private bool _isRemote = false;

        private RSAnchor _anchor;

        private Vector3 _lastPosition;

        private Quaternion _lastRotation;

        /// <summary>
        /// An Id that represents the prefab, shared between all builds of the scene
        /// </summary>
#if UNITY_EDITOR
        [ReadOnly]
#endif
        [SerializeField]
        private string _prefabId = "";

        private byte[] _auxData;
        public byte[] AuxData
        {
            get { return _auxData; }
            set
            {
                _auxData = value;
                if (IsRegistered()) {
                    EntitiesManager.Instance.UpdateEntityData(gameObject, _anchor);
                }
            }
        }


        public ulong Id
        {
            get { return (ulong)_instanceId; }
            set { _instanceId = (long)value; }
        }

        public string PrefabId
        {
            get { return _prefabId; }
        }

        private bool IsRegistered()
        {
            return _isRegistered || _isRemote;
        }

        private bool TransformHasChanged()
        {
            return transform.hasChanged && 
                   (!transform.position.Equals(_lastPosition) ||
                    !transform.rotation.Equals(_lastRotation));
        }

        private void Register()
        {
            var aux_len = _auxData != null ? _auxData.Length : 0;
            Debug.Log("[LibResight] SnappedObject::Register(): instanceId=" + Id + " prefabId=" + PrefabId + ", AugData=" + aux_len);
            var anchor = EntitiesManager.Instance.AddAnchor(Id, Matrix4x4.identity);
            EntitiesManager.Instance.AddEntity(gameObject, anchor);
            if (transform == null || gameObject == null) {
                Debug.Log("NULL");
                return;
            }
            _anchor = anchor;
            _isRegistered = true;
            _lastPosition = transform.position;
            _lastRotation = transform.rotation;
        }

        private void OnStatus(EngineState state, IntPtr ctx)
        {
            if (state == EngineState.Mapping && !_isRegistered) {
                Register();

                LibResight.Instance.OnStatus -= OnStatus;
            }
        }

        // Use this for initialization
        void Start()
        {
            if (_instanceId == 0L) {
                _instanceId = GenerateId();

                if (LibResight.State == EngineState.Mapping) {
                    Register();
                    return;
                }

                LibResight.Instance.OnStatus += OnStatus;
            } else {
                _isRemote = true;
                _lastPosition = transform.position;
                _lastRotation = transform.rotation;
            }
        }

        // Update is called once per frame
        void LateUpdate()
        {
            if (IsRegistered() && TransformHasChanged()) {
                _lastPosition = transform.position;
                _lastRotation = transform.rotation;
                EntitiesManager.Instance.UpdateEntityPose(gameObject, _anchor);
            }
        }

        void OnDestroy()
        {
            LibResight.Instance.OnStatus -= OnStatus;
            // We only remove entities that were explicitly destroyed by the user
            if (!isQuitting && IsRegistered()) {
                EntitiesManager.Instance.RemoveEntity(gameObject, _anchor);
            }
        }

        internal static long GenerateId()
        {
            var random = new System.Random();
            var longBytes = new byte[8];
            random.NextBytes(longBytes);
            return BitConverter.ToInt64(longBytes, 0);
        }
    }
}