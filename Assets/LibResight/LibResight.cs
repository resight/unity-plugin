using System;
using System.IO;
using System.Runtime.InteropServices;
using AOT;
using UnityEngine;
using UnityEngine.XR.ARFoundation;
using UnityEngine.XR.ARSubsystems;
using Resight.Utilities;
using Resight.Utilities.Extensions;

namespace Resight
{
    // Native delegates
    public delegate void RSOnStatus(EngineState status, IntPtr ctx);
    public delegate void RSOnAnchor(RSAnchor anchor, int anchorType, byte weight, IntPtr userdata, IntPtr ctx);
    public delegate void RSOnEntityAdded(RSEntity entity, IntPtr ctx);
    public delegate void RSOnEntityRemoved(RSEntity entity, IntPtr ctx);
    public delegate void RSOnEntityPoseUpdated(RSEntity entity, IntPtr ctx);
    public delegate void RSOnEntityDataUpdated(RSEntity entity, IntPtr ctx);
    public delegate void RSOnMeshBlockAdded(RSMeshBlockAddedEvent blockAddedEvent, IntPtr ctx);
    public delegate void RSOnMeshBlockRemoved(ulong block_id, IntPtr ctx);
    public delegate void RSOnMeshBlockUpdated(ulong block_id, IntPtr ctx);

    public enum EngineState
    {
        Uninitialized = -1,
        Init = 0,
        Mapping,
        Stopping,
        Stopped
    }

    [StructLayout(LayoutKind.Sequential)]
    public struct RSPosition
    {
        public float x;
        public float y;
        public float z;
    }

    [StructLayout(LayoutKind.Sequential)]
    public struct RSRotation
    {
        public float x;
        public float y;
        public float z;
        public float w;
    }

    [StructLayout(LayoutKind.Sequential)]
    public struct RSPose
    {
        public RSPosition pos;
        public RSRotation rot;
    }

    [StructLayout(LayoutKind.Sequential)]
    public struct RSAnchor
    {
        public ulong id;
        public ulong parentId;
        public RSPose pose;
    }

    [StructLayout(LayoutKind.Sequential)]
    public struct RSEntity
    {
        [MarshalAs(UnmanagedType.LPStr)]
        public string id;
        public ulong parentId;
        public RSPose pose;
        public ulong dtSize;
        [MarshalAs(UnmanagedType.ByValArray, ArraySubType = UnmanagedType.U1, SizeConst = 512)]
        public byte[] data;
    }

    [StructLayout(LayoutKind.Sequential)]
    public struct RSMeshBlockAddedEvent
    {
        public ulong block_id;
        public RSPosition block_position;
        public RSPosition block_size;
    }

    [StructLayout(LayoutKind.Sequential)]
    public struct RSMeshBlockVertexElement
    {
        public RSPosition position;
        public RSPosition normal;
        public RSRotation color;
    }

    [StructLayout(LayoutKind.Sequential)]
    public unsafe struct RSMeshBlockTriangle
    {
        public fixed uint vertices[3];
    }

    [StructLayout(LayoutKind.Sequential)]
    public struct RSConfiguration
    {
        [MarshalAs(UnmanagedType.LPStr)]
        public string dataPath;

        [MarshalAs(UnmanagedType.LPStr)]
        public string libPath;

        [MarshalAs(UnmanagedType.LPStr)]
        public string developerKey;

        [MarshalAs(UnmanagedType.LPStr)]
        public string ns;

        public RSOnStatus OnStatus;
        public RSOnAnchor OnAnchor;

        public RSOnEntityAdded OnEntityAdded;
        public RSOnEntityRemoved OnEntityRemoved;
        public RSOnEntityPoseUpdated OnEntityPoseUpdated;
        public RSOnEntityDataUpdated OnEntityDataUpdated;

        public RSOnMeshBlockAdded OnMeshBlockAdded;
        public RSOnMeshBlockRemoved OnMeshBlockRemoved;
        public RSOnMeshBlockUpdated OnMeshBlockUpdated;

        public IntPtr ctx;
    }

    [StructLayout(LayoutKind.Sequential)]
    public struct RSLightEstimate
    {
        public float ambient_intensity;
        public float ambient_color_temperature;
    }

    [StructLayout(LayoutKind.Sequential)]
    public struct RSPixelType
    {
        public int type;
    }

    [StructLayout(LayoutKind.Sequential)]
    public unsafe struct RSCamera
    {
        public RSPose pose;
        public fixed float intrinsics[4];
        public fixed float image_resolution[2];
        public float exposure_duration;
        public float exposure_offset;
    }

    [StructLayout(LayoutKind.Sequential)]
    public struct RSBuffer
    {
        public IntPtr buf;
        public int width;
        public int height;
        public int stride;
        public RSPixelType pixel_type;
    }

    [StructLayout(LayoutKind.Sequential)]
    public struct RSFrameEvent
    {
        public ulong frame_ts;
        public int tracking_state;
        public RSCamera camera;
        public RSBuffer yplane;
        public RSBuffer uvplane;
        public RSBuffer depth_plane;
        public RSBuffer depth_confidence_plane;
        public RSLightEstimate light_estimate;
    }

    [DefaultExecutionOrder(ARUpdateOrder.k_Session + 4)]
    public class LibResight : MonoBehaviour
    {
        public string devKey;
        public string nameSpace;
        [SerializeField] ARCameraManager cameraManager;

        public event RSOnStatus OnStatus;
        public event RSOnAnchor OnAnchor;
        public event RSOnEntityAdded OnEntityAdded;
        public event RSOnEntityRemoved OnEntityRemoved;
        public event RSOnEntityPoseUpdated OnEntityPoseUpdated;
        public event RSOnEntityDataUpdated OnEntityDataUpdated;
        public event RSOnMeshBlockAdded OnMeshBlockAdded;
        public event RSOnMeshBlockRemoved OnMeshBlockRemoved;
        public event RSOnMeshBlockUpdated OnMeshBlockUpdated;

        public static LibResight Instance { get; private set; }
        public static EngineState State { get; private set; } = EngineState.Uninitialized;

        private bool running_ = false;
        private AROcclusionManager occlusionManager_;

        void Awake()
        {
            Debug.Log("[LibResight] Awake()");

            if (Instance != null && Instance != this) {
                Debug.LogError("[LibResight] Only one instance of LibResight is allowed");
                return;
            }

            Instance = this;

            //Application.targetFrameRate = 60;
        }

        void Start()
        {
            Debug.Log("[LibResight] Start()");

            // The only way to access the depth texture in ARFoundation is to use the AROcclusionManager.
            // However, when it resides in the ARCamera, it will use the depth to occlude
            // and it interferes when you want to use a mesh (z fighting).
            // When AROcclusionManager is not attached to the camera, depth textures are available,
            // but depth-texture-occlusion will not be used.
            occlusionManager_ = FindObjectOfType<AROcclusionManager>();
            if (!occlusionManager_) {
                occlusionManager_ = gameObject.AddComponent<AROcclusionManager>();
                occlusionManager_.requestedEnvironmentDepthMode = EnvironmentDepthMode.Fastest;
                occlusionManager_.subsystem?.Start();
            }

            running_ = true;
            Init();
        }

        void Update()
        {
            RSTick(iOSTimeNow());
        }

        private void Init()
        {
            Debug.Log("[LibResight] Init()");
            MainQueue.Reset();

            var conf = new RSConfiguration();
            var timestamp = iOSTimeNow();
            var dataPath = Application.persistentDataPath;
            var dirInf = new DirectoryInfo(dataPath);
            if (!dirInf.Exists) {
                dirInf.Create();
            }

            conf.dataPath = dataPath;
            conf.libPath = Application.dataPath + "/..";
            conf.developerKey = devKey;
            conf.ns = nameSpace;
            conf.OnStatus = OnStatus_;
            conf.OnAnchor = OnAnchor_;
            conf.OnEntityAdded = OnEntityAdded_;
            conf.OnEntityRemoved = OnEntityRemoved_;
            conf.OnEntityPoseUpdated = OnEntityPoseUpdated_;
            conf.OnEntityDataUpdated = OnEntityDataUpdated_;
            conf.OnMeshBlockAdded = OnMeshBlockAdded_;
            conf.OnMeshBlockRemoved = OnMeshBlockRemoved_;
            conf.OnMeshBlockUpdated = OnMeshBlockUpdated_;
            conf.ctx = (IntPtr)0;

            Debug.Log("[LibResight] dataPath=" + conf.dataPath + " libPath=" + conf.libPath);

            RSInitialize(timestamp, conf);
            iOSLocationServicesStart();
        }

        void OnEnable()
        {
            Debug.Log($"[LibResight] OnEnable()");

            if (cameraManager != null) {
                cameraManager.frameReceived += OnARFrame;
            }
        }

        void OnDisable()
        {
            Debug.Log($"[LibResight] OnDisable()");

            if (cameraManager != null) {
                cameraManager.frameReceived -= OnARFrame;
            }
        }

        void OnApplicationPause(bool paused)
        {
            Debug.Log($"[LibResight] OnApplicationPause({paused})");

            if (State == EngineState.Init) {
                return;
            }

            if (paused) {
                Debug.Log("[LibResight] Stopping ResightSDK...");
                RSStop(iOSTimeNow());
                iOSLocationServicesStop();
            } else {
                if (!running_) {
                    Init();
                }
            }
        }

        unsafe void OnARFrame(ARCameraFrameEventArgs events)
        {
            if (!cameraManager) { return; }

            switch (Screen.orientation) {
                case ScreenOrientation.Portrait:
                    break;
                case ScreenOrientation.LandscapeLeft:
                    break;
                default:
                    Debug.LogError("[LibResight] ReSight only supports left-landscape/portrait orientation. Ignoring frames...");
                    return;
            }

            switch (ARSession.state) {
                case ARSessionState.SessionTracking:
                    break;
                default:
                    return;
            }

            switch (State) {
                case EngineState.Uninitialized:
                    Debug.LogError("[LibResight] ResightSDK hasn't been initialized. Ignoring frame");
                    return;
            }

            if (!cameraManager.TryGetIntrinsics(out XRCameraIntrinsics intrinsics)) {
                Debug.LogError("[LibResight] Couldn't get camera intrinsics. Ignoring frame");
                return;
            }

            var frameEvent = new RSFrameEvent
            {
                frame_ts = iOSTimeNow(),
                tracking_state = 1
            };

            frameEvent.camera.pose = cameraManager.transform.ToRSPose();
            frameEvent.camera.intrinsics[0] = intrinsics.focalLength.x;
            frameEvent.camera.intrinsics[1] = intrinsics.focalLength.y;
            frameEvent.camera.intrinsics[2] = intrinsics.principalPoint.x;
            frameEvent.camera.intrinsics[3] = intrinsics.principalPoint.y;
            frameEvent.camera.exposure_duration = (float)(events.exposureDuration ?? 0);
            frameEvent.camera.exposure_offset = events.exposureOffset ?? 0;
            frameEvent.light_estimate.ambient_intensity = events.lightEstimation.averageIntensityInLumens ?? 0;
            frameEvent.light_estimate.ambient_color_temperature = events.lightEstimation.averageColorTemperature ?? 0;

            var yuv = new XRCpuImage();
            var depth = new XRCpuImage();
            var depth_confidence = new XRCpuImage();

            if (cameraManager.TryAcquireLatestCpuImage(out yuv)) {
                frameEvent.yplane = yuv.ToRSBuffer(0);
                frameEvent.uvplane = yuv.ToRSBuffer(1);
            }
            else {
                Debug.LogError("[LibResight] Couldn't get image of frame. Ignoring frame");
                return;
            }

            frameEvent.camera.image_resolution[0] = yuv.width;
            frameEvent.camera.image_resolution[1] = yuv.height;

            if (occlusionManager_ && occlusionManager_.TryAcquireEnvironmentDepthCpuImage(out depth)) {
                frameEvent.depth_plane = depth.ToRSBuffer(0);
            }

            if (occlusionManager_ && occlusionManager_.TryAcquireEnvironmentDepthConfidenceCpuImage(out depth_confidence)) {
                frameEvent.depth_confidence_plane = depth_confidence.ToRSBuffer(0);
            }

            RSOnFrame(iOSTimeNow(), frameEvent);

            yuv.Dispose();
            depth.Dispose();
            depth_confidence.Dispose();
        }
        
        [MonoPInvokeCallback(typeof(RSOnStatus))]
        static void OnStatus_(EngineState status, IntPtr ctx)
        {
            Debug.Log($"[LibResight] OnStatus_(): {status}");

            if (status == EngineState.Stopped) {
                Debug.Log("[LibResight] OnStatus(): Stopped -> tearing down");
                RSTearDown();
                Instance.running_ = false;
            }

            MainQueue.Enqueue(() => {
                State = status;
                Debug.Log($"[LibResight] OnStatus(): {State}");
                Instance.OnStatus?.Invoke(status, ctx);
            });
        }

        [MonoPInvokeCallback(typeof(RSOnAnchor))]
        static void OnAnchor_(RSAnchor anchor, int anchorType, byte weight, IntPtr userdata, IntPtr ctx)
        {
            MainQueue.Enqueue(() => {
                Instance.OnAnchor?.Invoke(anchor, anchorType, weight, userdata, ctx);
            });
        }

        [MonoPInvokeCallback(typeof(RSOnEntityAdded))]
        static void OnEntityAdded_(RSEntity entity, IntPtr ctx)
        {
            MainQueue.Enqueue(() => {
                Instance.OnEntityAdded?.Invoke(entity, ctx);
            });
        }

        [MonoPInvokeCallback(typeof(RSOnEntityRemoved))]
        static void OnEntityRemoved_(RSEntity entity, IntPtr ctx)
        {
            MainQueue.Enqueue(() => {
                Instance.OnEntityRemoved?.Invoke(entity, ctx);
            });
        }

        [MonoPInvokeCallback(typeof(RSOnEntityPoseUpdated))]
        static void OnEntityPoseUpdated_(RSEntity entity, IntPtr ctx)
        {
            MainQueue.Enqueue(() => {
                Instance.OnEntityPoseUpdated?.Invoke(entity, ctx);
            });
        }

        [MonoPInvokeCallback(typeof(RSOnEntityDataUpdated))]
        static void OnEntityDataUpdated_(RSEntity entity, IntPtr ctx)
        {
            MainQueue.Enqueue(() => {
                Instance.OnEntityDataUpdated?.Invoke(entity, ctx);
            });
        }

        [MonoPInvokeCallback(typeof(RSOnMeshBlockAdded))]
        static void OnMeshBlockAdded_(RSMeshBlockAddedEvent blockAddedEvent, IntPtr ctx)
        {
            MainQueue.Enqueue(() => {
                Instance.OnMeshBlockAdded?.Invoke(blockAddedEvent, ctx);
            });
        }

        [MonoPInvokeCallback(typeof(RSOnMeshBlockRemoved))]
        static void OnMeshBlockRemoved_(ulong block_id, IntPtr ctx)
        {
            MainQueue.Enqueue(() => {
                Instance.OnMeshBlockRemoved?.Invoke(block_id, ctx);
            });
        }

        [MonoPInvokeCallback(typeof(RSOnMeshBlockUpdated))]
        static void OnMeshBlockUpdated_(ulong block_id, IntPtr ctx)
        {
            MainQueue.Enqueue(() => {
                Instance.OnMeshBlockUpdated?.Invoke(block_id, ctx);
            });
        }

#if UNITY_IOS && !UNITY_EDITOR
        // Native functions
        [DllImport("__Internal")]
        private static extern void RSInitialize(ulong ts, RSConfiguration conf);

        [DllImport("__Internal")]
        private static extern void RSTearDown();

        [DllImport("__Internal")]
        private static extern void RSStop(ulong ts);

        [DllImport("__Internal")]
        private static extern EngineState RSGetState();

        [DllImport("__Internal")]
        private static extern void RSTick(ulong ts);

        [DllImport("__Internal")]
        private static extern void RSOnFrame(ulong ts, RSFrameEvent frameEvent);

        [DllImport("__Internal")]
        private static extern void iOSLocationServicesStart();

        [DllImport("__Internal")]
        private static extern void iOSLocationServicesStop();

        [DllImport("__Internal")]
        private static extern ulong iOSTimeNow();
#else
        private static void RSInitialize(ulong ts, RSConfiguration conf) {}

        private static void RSTearDown() {}

        private static void RSStop(ulong ts) {}

        private static EngineState RSGetState() {return EngineState.Mapping; }

        private static void RSTick(ulong ts) {}

        private static void RSOnFrame(ulong ts, RSFrameEvent frameEvent) {}

        private static void iOSLocationServicesStart() {}

        private static void iOSLocationServicesStop() {}

        private static ulong iOSTimeNow() {return 0;}
#endif
    }


} // namespace Resight
